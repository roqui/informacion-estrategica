@extends('layouts/plantilla')
@extends('layouts/menu')
@section('title','Registros')

@section('loader')
<div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="../../../resources/dist/img/logo.png" alt="AdminLTELogo" height="100"
        width="200">
</div>
@endsection

@section('css')
<!-- DataTables -->
<link rel="stylesheet" href="../../../resources/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../../../resources/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="../../../resources/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
@endsection

@section('subtitle')
<!-- Content Header (Page header) -->
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Consultar Registros de Información Estratégica</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                    <li class="breadcrumb-item">Información Estratégica</li>
                    <li class="breadcrumb-item active">Registros</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->
@endsection



@section('main')
<section class="content">
    <div class="card card-primary">
        <div class="card-header" style="background-color: #6c6c74;">
            <div class="row">
                <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                    <h3 class="card-title">Consulte los registros de Información Estratégica.</h3>
                </div>
                @if (rol('administrador'))
                <div class="col-xl-6 col-lg-6">
                    <form action="{{ route('registros') }}" method="GET" enctype="multipart/form-data">
                        <div class="d-flex justify-content-end">
                            <select class="select2bs4 col-lg-12 col-sm-8" id="dependencia" name="dependencia">
                                <option value="">{{$dep}}</option>
                                <optgroup label="Dependencias">
                                    @foreach (getDependencias() as $dependencia)
                                    <option value="{{$dependencia}}" class="text-wrap">{{$dependencia}}</option>
                                    @endforeach
                                </optgroup>
                                <!-- <optgroup label="Municipios">
                                    @foreach (getMunicipios() as $municipio)
                                    <option value="{{$municipio[1]}}">{{$municipio[1]}}</option>
                                    @endforeach
                                </optgroup> -->
                            </select>
                            <button class="btn btn-secondary ml-1">
                                Filtrar
                            </button>
                        </div>
                    </form>
                </div>
                @endif
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if($registros->count())
            <div class="table-responsive">
                <table id="tablaRegistros" class="table table-bordered table-striped" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Ent/Dep</th>
                            <th>Fecha</th>
                            <th>Periodo Rep.</th>
                            <th>Prioridad</th>
                            <th>Tema</th>
                            <th>Resp. Validación</th>
                            <th>Resp. Atención</th>
                            <th class="text-center"><i class="fas fa-info"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($registros as $registro)
                        <tr>
                            <td>{{ $registro->dependencia }}</td>
                            <td>{{ $registro->fecha }}</td>
                            <td>{{ $registro->periodo_reportado }}</td>
                            <td>{{ $registro->prioridad }}</td>
                            <td>{{ $registro->tema }}</td>
                            <td>{{ $registro->responsable_validacion }}</td>
                            <td>{{ $registro->responsable_atencion }}</td>
                            <td><button class="btn btn-info"
                                    onclick="$('#exampleModalCenter{{$registro->id}}').modal('show');"> Ver más
                                </button>
                            </td>
                        </tr>

                        <!-- Modal -->
                        <div class="modal fade bd-example-modal-lg" id="exampleModalCenter{{$registro->id}}"
                            tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Registro</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="card card-primary">
                                            <div class="card-header" style="background-color: #6c6c74;">
                                                <h3 class="card-title">Consulte la información general de este registro.
                                                </h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body">
                                                <ul class="list-group">
                                                    <li class="list-group-item"><strong>Dependecia / Entidad: </strong>
                                                        {{$registro->dependencia}} </li> <br>
                                                    <li class="list-group-item"><strong>Fecha de envío: </strong>
                                                        {{$registro->fecha}} </li> <br>
                                                    <li class="list-group-item"><strong>Periodo que reporta: </strong>
                                                        {{$registro->periodo_reportado}}</li> <br>
                                                    <li class="list-group-item"><strong>Responsable de validación:
                                                        </strong>
                                                        {{$registro->responsable_validacion}}</li> <br>
                                                    <li class="list-group-item"><strong>Prioridad: </strong>
                                                        {{$registro->prioridad}}</li> <br>
                                                    <li class="list-group-item"><strong>Tema: </strong>
                                                        {{$registro->tema}}
                                                    </li> <br>
                                                    <li class="list-group-item"><strong>Responsable de atención:
                                                        </strong>
                                                        {{$registro->responsable_atencion}}</li> <br>
                                                    <li class="list-group-item"><strong>Problemática: </strong>
                                                        {{$registro->problematica}}
                                                        @if ($registro->ruta_problematica != "" &&
                                                        $registro->ruta_problematica != null)
                                                        <form action="{{ route('download_file') }}" method="POST">
                                                            @csrf
                                                            <input type="text" value="{{$registro->ruta_problematica}}"
                                                                name="path" hidden>
                                                            <button class="btn btn-link" data-toggle="tooltip"
                                                                title="{{$registro->nombre_problematica}}">
                                                                {{$registro->nombre_problematica}}
                                                            </button>
                                                        </form>
                                                        @else
                                                        <small class="text-muted">Sin archivo para descargar</small>
                                                        @endif
                                                    </li> <br>
                                                    <li class="list-group-item"><strong>Áreas: </strong>
                                                        {{$registro->areas}}</li>
                                                    <li class="list-group-item"><strong>Organizaciones y/o actores
                                                            involucrados: </strong> {{$registro->organizaciones}}</li>
                                                    <br>
                                                    <li class="list-group-item"><strong>Seguimiento: </strong>
                                                        {{$registro->seguimiento}}</li> <br>
                                                    <li class="list-group-item"><strong>Propuestas de acción: </strong>
                                                        {{$registro->propuestas}}
                                                        @if ($registro->ruta_propuestas != "" &&
                                                        $registro->ruta_propuestas
                                                        != null)
                                                        <form action="{{ route('download_file') }}" method="POST">
                                                            @csrf
                                                            <input type="text" value="{{$registro->ruta_propuestas}}"
                                                                name="path" hidden>
                                                            <button class="btn btn-link" data-toggle="tooltip"
                                                                title="{{$registro->nombre_propuestas}}">
                                                                {{$registro->nombre_propuestas}}
                                                            </button>
                                                        </form>
                                                        @else
                                                        <small class="text-muted">Sin archivo para descargar</small>
                                                        @endif
                                                    </li> <br>
                                                    <li class="list-group-item"><strong>Impacto probable: </strong>
                                                        @if ($registro->impacto_probable == "Municipal")
                                                        {{$registro->impacto_probable}}:
                                                        {{ $registro->impacto_dependencia }}
                                                        @else
                                                        {{$registro->impacto_probable}}
                                                        @endif
                                                    </li>
                                                    <br>
                                                    <li class="list-group-item"><strong>Riesgos: </strong>
                                                        {{$registro->riesgos}}</li>
                                                    <br>

                                                </ul>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Ent/Dep</th>
                            <th>Fecha</th>
                            <th>Periodo Rep.</th>
                            <th>Prioridad</th>
                            <th>Tema</th>
                            <th>Resp. Validación</th>
                            <th>Resp. Atención</th>
                            <th class="text-center"><i class="fas fa-info"></i></th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            @else
                @if (rol('administrador'))
                <h1 class="text-center" style="text-transform: capitalize;">No hay solicitudes de {{mb_strtolower($dep)}}</h1>
                @else
                <h1 class="text-center" style="text-transform: capitalize;">No hay solicitudes de {{mb_strtolower(Auth::user()->name)}}</h1>
                @endif
            @endif
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->

</section>
@endsection


@section('js')
<script src="{{ asset('resources/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="{{ asset('resources/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script> -->
<script src="{{ asset('resources/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('resources/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>



<script>
console.log("hola");
$(document).ready(function() {
    $('#tablaRegistros').DataTable({
        "language": {
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "zeroRecords": "Sin registros que mostrar",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "No archivos disponibles",
            "infoFiltered": "(Encontrado total de _MAX_ Registros)",
            "search": "Buscar:",
            "paginate": {
                "first": "Primera",
                "last": "Última",
                "next": "Siguiente",
                "previous": "Anterior"
            },
        },
        responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        columnDefs: [{
            className: 'control',
            orderable: false,
            // targets:   0 // oculta la primer linea
        }],
        order: [0, 'asc']
    });
});
</script>

@endsection