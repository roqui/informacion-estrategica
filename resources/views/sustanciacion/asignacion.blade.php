@extends('layouts/plantilla')
@extends('layouts/menu')
@section('title','SISI')

@section('subtitle')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">Asignación de Expedientes</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Home</a></li>
                <li class="breadcrumb-item active">Sutentación</li>
                <li class="breadcrumb-item active">Asignación de Expendientes</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
@endsection

@section('main')
<section class="content">
    <div class="card card-primary">
        <div class="card-header" style="background-color: #6c6c74;" >
          <h3 class="card-title">Ingrese los campos correspondientes del expediente.</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form autocomplete="off">
          <div class="card-body">

            <div class="form-group">
                <label>Fecha de recepción:</label>
                  <div class="input-group date" id="reservationdate" data-target-input="nearest">
                      <input type="date" class="form-control datetimepicker-input" data-target="#reservationdate"/>
                      <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                          <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                      </div>
                  </div>
              </div>

              <div class="form-group">
                <label>Minimal (.select2-danger)</label>
                <select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
                  <option selected="selected">Alabama</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>

            <div class="form-group">
                <label>Municipio</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected">Morelia</option>
                  <option>Alaska</option>
                  <option>California</option>
                  <option>Delaware</option>
                  <option>Tennessee</option>
                  <option>Texas</option>
                  <option>Washington</option>
                </select>
              </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Password</label>
              <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            </div>
            <div class="form-group">
              <label for="exampleInputFile">File input</label>
              <div class="input-group">
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="exampleInputFile">
                  <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text">Upload</span>
                </div>
              </div>
            </div>

          </div>
          <!-- /.card-body -->

          <div class="card-footer">
            <button type="button" class="btn btn-secondary" style="float: right;">Guardar</button>
          </div>
        </form>
      </div>
      <!-- /.card -->
</section>
@endsection
