<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Información Estratégica</title>
        <link rel="icon" type="image/x-icon" href="{{asset('resources/form/assets/honestidad_y_trabajo.png')}}" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="{{ asset('resources/form/css/styles.css')}}" rel="stylesheet" />
        <link rel="stylesheet" href="{{ asset('resources/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('resources/plugins/select2/css/select2.min.css') }}">
        <link rel="stylesheet" href="{{ asset('resources/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
        <script type="text/javascript">

            function callbackThen(response){
                // read HTTP status
                console.log(response.status);
                // read Promise object
                response.json().then(function(data){
                console.log(data);
                });
            }
        
            function callbackCatch(error){
                console.error('Error:', error)
            }
            </script>
    
            {!! htmlScriptTagJsApi([
            'callback_then' => 'callbackThen',
            'callback_catch' => 'callbackCatch'
            ]) !!}
            
    </head>
    <style>
        .boton{
            background-color: #f1bdcc;
            color: #4b041b;
        }
        .boton:hover{
            background-color: #4b041b;
            color: #f1bdcc;
        }
        .footer-pleca{
            background-color: #6A0F49;
            background-image: url(https://michoacan.gob.mx/images/pleca.svg);
            background-repeat: repeat-x;
        }
        .nav-main{
            background-color:#EEEDEE; 
            -webkit-box-shadow: 0px 9px 0px 9px rgba(75,4,27,1);
            -moz-box-shadow: 0px 9px 0px 9px rgba(75,4,27,1);
            box-shadow: 0px 9px 0px 9px rgba(75,4,27,1);
            color:#4b041b;
        }
        span{
            style="color: #4b041b"
        }
    </style>
    <body id="page-top">
        <!-- Navigation-->
        <nav class="navbar navbar-expand-lg nav-main" id="mainNav" style="">
            <div class="container px-4">
                {{-- <a class="navbar-brand" href="#">
                    <img src="{{asset('resources/form/assets/logo.png')}}" width="30" height="30" alt="">
                </a> --}}
                <a class="navbar-brand" style="color: #4b041b" href="#page-top">
                    <img src="{{asset('resources/form/assets/gob-mich-horizontal.png')}}" width="200" alt="">
                </a>
                <button style="color: #4b041b" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ms-auto">

                        {{-- <li class="nav-item"><h6><a style="color: #4b041b" class="nav-link" href="#about">ACERCA</a></h6></li>
                        <li class="nav-item"><H6><a style="color: #4b041b" class="nav-link" href="#services">SERVICIOS</a></H6></li>
                        <li class="nav-item"><h6><a style="color: #4b041b" class="nav-link" href="#contact">INFORMACIÓN</a></h6></li> --}}

                        <li class="nav-item"><H6><a style="color: #4b041b" class="nav-link" href="{{ route('dashboard') }}">Regresar al Panel de Control <i class="fas fa-arrow-right"></i></a></H6></li>

                    </ul>
                </div>
            </div>
        </nav>
        <!-- Header-->
        {{-- <header class="bg-primary bg-gradient text-white">
            <div class="container px-4 text-center">
                <h1 class="fw-bolder">Welcome to Scrolling Nav</h1>
                <p class="lead">A functional Bootstrap 5 boilerplate for one page scrolling websites</p>
                <a class="btn btn-lg btn-light" href="#about">Start scrolling!</a>
            </div>
        </header> --}}
        <!-- About section-->
        {{-- <section id="about">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>About this page</h2>
                        <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
                        <ul>
                            <li>Clickable nav links that smooth scroll to page sections</li>
                            <li>Responsive behavior when clicking nav links perfect for a one page website</li>
                            <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
                            <li>Minimal custom CSS so you are free to explore your own unique design options</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- Services section-->
        <section class="bg-light" id="services">
            <div class="container px-4">

                {{-- <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>Services we offer</h2>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut optio velit inventore, expedita quo laboriosam possimus ea consequatur vitae, doloribus consequuntur ex. Nemo assumenda laborum vel, labore ut velit dignissimos.</p>
                    </div>
                    
                </div> --}}
                
                <form action="{{ route('formulario_ie_captura') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <div class="row text-center">
                                <h2 style="color: #4b041b"><strong>FICHA DE INFORMACIÓN ESTRATÉGICA</strong></h2>
                            </div>
                            <hr>
                            <strong><small class="text-muted">Los campos obligatorios se encuentran marcados con </small> <small class="text-danger">*</small></strong>. 
                            <div class="row">
                                <div class="col-10 m-1 bg-white border rounded-lg">

                                    @if (rol('administrador'))
                                    <div class="input-group">
                                    
                                        <label class="input-group-text" for="dependencia" style="color: #4b041b"><strong>Dependencia<small class="text-danger">*</small></strong></label>
                                        
                                        <select class="select2bs4" id="dependencia" name="dependencia" required>
                                          <option value ="" >-- Seleccione una dependencia --</option>
                                          <optgroup label="Dependencias">
                                              @foreach (getDependencias() as $dependencia)
                                                  <option value="{{$dependencia}}" class="text-wrap">{{$dependencia}}</option>
                                              @endforeach
                                          </optgroup>
                                          <!-- <optgroup label="Municipios">
                                              @foreach (getMunicipios() as $municipio)
                                                  <option value="{{$municipio[1]}}">{{$municipio[1]}}</option>
                                              @endforeach
                                          </optgroup> -->
                                        </select>
                                    
                                    </div>
                                    @else
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" style="color: #4b041b">Dependencia o Municipio <small class="text-danger">*</small></span>
                                        </div>
                                        <input type="text" class="form-control disabled" name="dependencia" readonly required value="{{ Auth::user()->name }}">
                                    </div> 
                                    @endif

                                    
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" style="color: #4b041b"><strong>Fecha de envío <small class="text-danger">*</small></strong></span>
                                        </div>
                                        <input type="date" class="form-control disabled" name="fecha" readonly required value="{{ getToday() }}">
                                    </div>
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" style="color: #4b041b"><strong>Periodo que reporta <small class="text-danger">*</small></strong></span>
                                        </div>
                                        <input type="text" class="form-control disabled" name="periodo" readonly required value="{{ getWeek() }}">
                                    </div>
                                    <div class="input-group input-group-sm mb-1">
                                        <div class="input-group-prepend">
                                          <span class="input-group-text" style="color: #4b041b"><strong>Responsable de validación <small class="text-danger">*</small></strong></span>
                                        </div>
                                        <input type="text" class="form-control" name="responsable_validacion" placeholder="Ingrese aquí un responsable de validación" required>
                                    </div>
                                </div>
                                
                                <div class="col m-1 bg-white border rounded-lg" >
                                    <strong><label style="color: #4b041b">Prioridad <small class="text-danger">*</small></label></strong><br>
                                    <small class="text-muted">Seleccione la prioridad que desea que se le de a esta ficha estratégica.</small>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="prioridad" id="prioridad_alta" value="alta" required>
                                        <label class="form-check-label" for="prioridad_alta">
                                            Alta
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="prioridad" id="prioridad_baja" value="baja" required>
                                        <label class="form-check-label" for="prioridad_baja">
                                          Baja
                                        </label>
                                    </div>
                                </div>
                                
                            </div>
                        </div>

                        <div class="card-body">
                          
                            <div class="card">

                                <div class="card-header">

                                    <div class="row">
                                        <div class="col">
                                            <strong><label for="tema" style="color: #4b041b">Tema <small class="text-danger">*</small></label></strong>
                                            <textarea class="form-control" rows="1" id="tema" name="tema" placeholder="Ingrese aquí el tema de esta ficha" required></textarea>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <strong><label for="responsable_atencion" style="color: #4b041b"> Responsable de atención <small class="text-danger">*</small></label></strong>
                                                <input type="text" class="form-control" id="responsable_atencion" name="responsable_atencion" placeholder="Ingrese aquí el responsable de atención" required>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="row m-1">
                                        <strong><label for="problematica" style="color: #4b041b">Breve descripción del tema <small class="text-danger">*</small></label></strong>
                                        <textarea class="form-control" rows="5" id="problematica" placeholder="Ingrese aquí una breve explicación" name="problematica" required></textarea>
                                        <input class="m-1" type="file" id="archivo_problematica" name="archivo_problematica">
                                        <small class="text-muted">Aquí puede adjuntar un archivo relacionado a la problemática. Este archivo no es obligatorio.</small>
                                    </div>
                                    <br>


                                    <div class="row m-1">
                                        <strong><label for="areas" style="color: #4b041b">Áreas que participan <small class="text-danger">*</small></label></strong>
                                        <textarea class="form-control" rows="2" id="areas" name="areas" placeholder="Ingrese aquí las áreas involucradas" required></textarea>
                                    </div>
                                </div>
                                
                            </div>

                            <div class="row mt-2">
                                <div class="col">
                                    <div class="card card-header">
                                        <strong><label for="organizaciones" style="color: #4b041b">Organizaciones y/o actores involucrados <small class="text-danger">*</small></label></strong>
                                        <textarea class="form-control" rows="5" id="organizaciones" name="organizaciones" placeholder="Ingrese aquí las organizaciones y/o actores involucrados (nombres y cargos)" required></textarea>
                                    </div>
                                    
                                    <div class="card card-header mt-2">
                                        <div class="card">
                                            <div class="row m-1">
                                                <div class="col">
                                                    <strong><label for="seguimiento" style="color: #4b041b">Seguimiento <small class="text-danger">*</small></label></strong>
                                                    <textarea class="form-control" rows="5" id="seguimiento" name="seguimiento" placeholder="Ingrese aquí el estado correspondiente" required></textarea>
                                                </div>
                                                <div class="col">
                                                    
                                                    <strong><label for="recursos" style="color: #4b041b">Recursos financieros <small class="text-danger">*</small></label></strong>
                                                    <textarea class="form-control" rows="5" id="recursos" name="recursos" placeholder="Ingrese aquí los recursos distribuidos en su atención" required></textarea>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="row mt-2">
                                <div class="card card-header">
                                    <div class="input-group">
                                        <label class="input-group" for="impacto" style="color: #4b041b"><strong>Impacto probable <small class="text-danger">*</small></strong></label>
                                        <select class="select2bs4" id="impacto" name="impacto" required>
                                            <option value="">-- Seleccione una opción --</option>
                                            <option value="Estatal">Estatal</option>
                                            <option value="Municipal">Municipal</option>
                                        </select>
                                    </div>
                                    <div class="input-group" id="div_impacto_dependencia" hidden>
                                    
                                        <label class="input-group" for="impacto_dependencia" style="color: #4b041b"><strong> Municipio (Impacto Probable)<small class="text-danger">*</small></strong></label>
                                        
                                        <select class="select2bs4" id="impacto_dependencia" name="impacto_dependencia">
                                          <option value ="" >-- Seleccione un Municipio --</option>
                                          <optgroup label="Municipios">
                                              @foreach (getMunicipios() as $municipio)
                                                  <option value="{{$municipio[1]}}">{{$municipio[1]}}</option>
                                              @endforeach
                                          </optgroup>
                                        </select>
                                    
                                    </div>
                                    <div class="row">
                                        <strong><label for="riesgos" style="color: #4b041b">Posibles riesgos <small class="text-danger">*</small></label></strong>
                                        <textarea class="form-control" rows="2" id="riesgos" name="riesgos" placeholder="Ingrese aquí los posibles riesgos" required></textarea>
                                    </div>
                                    <div class="row mb-2">
                                        <strong><label for="propuestas" style="color: #4b041b">Propuestas de acción <small class="text-danger">*</small></label></strong>
                                        <textarea class="form-control" rows="2" id="propuestas" name="propuestas" placeholder="Ingrese aquí las propuestas para su atención" required></textarea>
                                        <input class="m-1" type="file" id="archivo_propuestas" name="archivo_propuestas">
                                        <small class="text-muted">Aquí puede adjuntar un archivo relacionado a las propuestas de acción. Este archivo no es obligatorio.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="card-footer" style="text-align: right">
                            <button class="btn btn-lg boton ">
                                Enviar
                            </button>
                        </div>
                    </div>

                    
                </form>
                
            </div>
        </section>
        <!-- Contact section-->
        {{-- <section id="contact">
            <div class="container px-4">
                <div class="row gx-4 justify-content-center">
                    <div class="col-lg-8">
                        <h2>Contact us</h2>
                        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vero odio fugiat voluptatem dolor, provident officiis, id iusto! Obcaecati incidunt, qui nihil beatae magnam et repudiandae ipsa exercitationem, in, quo totam.</p>
                    </div>
                </div>
            </div>
        </section> --}}
        <!-- Footer-->
        <footer class="footer-pleca">
            <br><br>
        </footer>
        <div class="row" style="background-color: #6c0c4c">
            <div class="container px-4 text-center"><small class="m-0 text-center" style="color: #f1bdcc">Derechos Reservador &copy; Fabrica Académica de Software. Instituto Tecnológico de Morelia. Gobierno  de Michoacán. 2021</small></div>
        </div>

        <script src="{{ asset('resources/plugins/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('resources/form/js/scripts.js') }}"></script>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.1.5/dist/sweetalert2.all.min.js"></script>
        <!-- Select2 -->
        <script src="{{ asset('resources/plugins/select2/js/select2.full.min.js') }}"></script>     
        <script>
            //Initialize Select2 Elements
            $('.select2').select2()
         
             //Initialize Select2 Elements
             $('.select2bs4').select2({
               theme: 'bootstrap4'
             })
        </script>
        <script>
            
            @if(session('success'))
            Swal.fire({
                icon: 'success',
                title: '',
                text: "{{ @session('success') }}",
                // footer: '<a href="">Why do I have this issue?</a>'
                });
            @endif

            @if ( session('error') )
            Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: "{{session('error')}}",
                    // footer: '<a href="">Why do I have this issue?</a>'
                    });
            @endif

            console.log("Hola desde información estratégica")
            document.getElementById('impacto').change = showDependencias;
            function showDependencias(event){
                let dependencia = document.getElementById('impacto');

                if(dependencia.value == 'Municipal'){
                    document.getElementById('div_impacto_dependencia').removeAttribute('hidden');
                    document.getElementById('impacto_dependencia').setAttribute('required','required');
                    
                }else{
                    document.getElementById('div_impacto_dependencia').setAttribute('hidden',true);
                    document.getElementById('impacto_dependencia').removeAttribute('required');
                }
            }

        </script>
    </body>
</html>
