@extends('layouts/plantilla')
@extends('layouts/menu')
@section('title','Panel de Control')

@section('subtitle')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0">PANEL DE CONTROL</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Inicio</a></li>
                <li class="breadcrumb-item active">Panel de Control</li>
              </ol>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->
@endsection

@section('main')


<section class="content">
  @if (rol('administrador'))
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        
        <!-- ./col -->
        <div class="col-lg col">
          <!-- small box -->
          <div class="small-box " style="background-color: #772b5b;">
            <div class="inner" style="color:white;">
              <h3>{{$cuenta_registros}}</h3>

              <p>Respuestas Registradas</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            {{-- <a href="{{route('registros')}}" class="small-box-footer">Ver más <i class="fas fa-arrow-circle-right"></i></a> --}}
          </div>
        </div>
        <!-- ./col -->
        {{-- <div class="col-lg- col">
          <!-- small box -->
          <div class="small-box" style="background-color: #772b5b;">
            <div class="inner" style="color:white;">
              <h3>3</h3>

              <p>Usuarios</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer disabled" disabled >Ver más (No disponible por el momento<i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div> --}}
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
          <div class="col">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Últimas 10 respuestas registradas:</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>

                </div>
                <!-- /.Ultimos Expedientes -->

                <div class="card-body p-0">
                  <ul class="products-list product-list-in-card pl-2 pr-2">
                    @isset($registros)
                      @foreach ($registros as $registro)
                      <li class="item">
                        <div class="product-img">
                          <img src="../images/release.png" class="img-size-50">
                        </div>
                        <div class="product-info">
                        <a class="product-title">{{$registro->id}}
                            <span class="badge badge-success float-right">{{date("d-m-Y", strtotime($registro->fecha))}}</span> </a><b>Tema: </b>{{ $registro->tema }}<br><b>&nbsp;&nbsp;&nbsp;&nbsp;Responsable de atención: </b>{{ $registro->responsable_atencion }}<br><b>&nbsp;&nbsp;&nbsp;&nbsp;Prioridad: </b>{{ $registro->prioridad }}<button class="btn btn-info float-right" onclick="$('#exampleModalCenter{{$registro->id}}').modal('show');"> Ver más </button>
                        </div>
                      </li>
                      
                      <!-- modal -->
                      <div class="modal fade bd-example-modal-lg" id="exampleModalCenter{{$registro->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Registro</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            </div>
                            <div class="modal-body">
                                <div class="card card-primary">
                                    <div class="card-header" style="background-color: #6c6c74;" >
                                      <h3 class="card-title">Consulte la información general de este registro.</h3>
                                    </div>
                                    <!-- /.card-header -->
                                    <div class="card-body">
                                        <ul class="list-group">
                                            <li class="list-group-item"><strong>Dependecia / Entidad: </strong> {{$registro->dependencia}} </li> <br>
                                            <li class="list-group-item"><strong>Fecha de envío: </strong> {{$registro->fecha}} </li> <br>
                                            <li class="list-group-item"><strong>Periodo que reporta: </strong> {{$registro->periodo_reportado}}</li> <br>
                                            <li class="list-group-item"><strong>Responsable de validación: </strong>  {{$registro->responsable_validacion}}</li> <br>
                                            <li class="list-group-item"><strong>Prioridad: </strong>  {{$registro->prioridad}}</li> <br>
                                            <li class="list-group-item"><strong>Tema: </strong> {{$registro->tema}}</li> <br>
                                            <li class="list-group-item"><strong>Responsable de atención: </strong> {{$registro->responsable_atencion}}</li> <br>
                                            <li class="list-group-item"><strong>Problemática: </strong> {{$registro->problematica}}
                                              @if ($registro->ruta_problematica != "" && $registro->ruta_problematica != null)
                                                <form action="{{ route('download_file') }}" method="POST">
                                                  @csrf
                                                  <input type="text" value="{{$registro->ruta_problematica}}" name="path" hidden>
                                                  <button class="btn btn-link" data-toggle="tooltip" title="{{$registro->nombre_problematica}}"> 
                                                    {{$registro->nombre_problematica}}
                                                  </button>
                                                </form>
                                              @else
                                                  <small class="text-muted">Sin archivo para descargar</small>
                                              @endif
                                            </li> <br>
                                            <li class="list-group-item"><strong>Áreas: </strong> {{$registro->areas}}</li>
                                            <li class="list-group-item"><strong>Organizaciones y/o actores involucrados: </strong>  {{$registro->organizaciones}}</li>  <br>
                                            <li class="list-group-item"><strong>Seguimiento: </strong> {{$registro->seguimiento}}</li> <br>
                                            <li class="list-group-item"><strong>Propuestas de acción: </strong> {{$registro->propuestas}}
                                              @if ($registro->ruta_propuestas != "" && $registro->ruta_propuestas != null)
                                                <form action="{{ route('download_file') }}" method="POST">
                                                  @csrf
                                                  <input type="text" value="{{$registro->ruta_propuestas}}" name="path" hidden>
                                                  <button class="btn btn-link" data-toggle="tooltip" title="{{$registro->nombre_propuestas}}"> 
                                                    {{$registro->nombre_propuestas}}
                                                  </button>
                                                </form>
                                              @else
                                                  <small class="text-muted">Sin archivo para descargar</small>
                                              @endif
                                            </li> <br>
                                            <li class="list-group-item"><strong>Impacto probable: </strong> 
                                              @if ($registro->impacto_probable == "Municipal")
                                                {{$registro->impacto_probable}}: {{ $registro->impacto_dependencia }}
                                              @else
                                                {{$registro->impacto_probable}}
                                              @endif
                                            </li>
                                            <br>
                                            <li class="list-group-item"><strong>Riesgos: </strong> {{$registro->riesgos}}</li> 
                                            <br>
                                            
                                        </ul>
                                      </div>
                                      <!-- /.card-body -->
                                </div>
                                  <!-- /.card -->
                            </div>
                            <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>
                        </div>
                      </div>
                      @endforeach
                    @endisset
                    <!-- /.item -->

                  </ul>
                </div>


                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <a href="{{route('registros')}}" class="uppercase">Ver todos los Registros</a>
                </div>
                <!-- /.card-footer -->
              </div>
            </div>



            {{-- <div class="col">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Usuarios registrados:</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>

                </div>
                <!-- /.Ultimos Expedientes -->

                <div class="card-body p-0">
                  <ul class="products-list product-list-in-card pl-2 pr-2">
                    <li class="item">
                      <div class="product-img">
                        <img src="../images/release.png" class="img-size-50">
                      </div>
                      <div class="product-info">
                        <a href="" class="product-title">26
                          <span class="badge badge-success float-right">Completo</span></a>Expediente de Juan Pérez<span class="product-description">

                      </span>
                      </div>
                    </li>
                    <!-- /.item -->

                  </ul>
                </div>


                <!-- /.card-body -->
                <div class="card-footer text-center">
                  <a href="" class="uppercase">Ver todos los Expedientes</a>
                </div>
                <!-- /.card-footer -->
              </div>
            </div> --}}
      </div>
      <!-- /.row (main row) -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-6">
              <!-- AREA CHART -->
              <div class="card card-primary" hidden>
                <div class="card-header">
                  <h3 class="card-title">Area Chart</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="areaChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- LINE CHART -->
              <div class="card card-info" hidden>
                <div class="card-header">
                  <h3 class="card-title">Line Chart</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="lineChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              

              <!-- PIE CHART -->
              <div class="card">
                <div class="card-header" style="background-color: #772b5b;">
                  <h3 class="card-title text-white">Gráfica de Prioridad</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <canvas id="pieChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
            <!-- /.col (LEFT) -->
            <div class="col-md-6">
              <!-- DONUT CHART -->
              <div class="card">
                <div class="card-header" style="background-color: #772b5b;">
                  <h3 class="card-title text-white" >Gráfica por Impacto Probable</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              

              <!-- BAR CHART -->
              <div class="card card-success" hidden>
                <div class="card-header">
                  <h3 class="card-title">Bar Chart</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

              <!-- STACKED BAR CHART -->
              <div class="card card-success" hidden>
                <div class="card-header">
                  <h3 class="card-title">Stacked Bar Chart</h3>

                  <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                      <i class="fas fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                      <i class="fas fa-times"></i>
                    </button>
                  </div>
                </div>
                <div class="card-body">
                  <div class="chart">
                    <canvas id="stackedBarChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                  </div>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->

            </div>
            <!-- /.col (RIGHT) -->
          </div>
          <!-- /.row -->
        </div><!-- /.container-fluid -->
      </section>

    </div><!-- /.container-fluid -->
  @else
  <div class="conainer-fluid">
    <div class="jumbotron jumbotron-fluid">
      <div class="container" style="font:">
        <h1 class="display-4">Bienvenido</h1>
        <p class="lead">Este es el Sistema para Registro de Información Estratégica.</p>
      </div>
  </div>
  </div>
  @endif

</section>
@endsection

@section('js')

  @if (rol('administrador'))
    <script>
      $(function () {
        /* ChartJS
        * -------
        * Here we will create a few charts using ChartJS
        */

        //--------------
        //- AREA CHART -
        //--------------

        // Get context with jQuery - using jQuery's .get() method.
        var areaChartCanvas = $('#areaChart').get(0).getContext('2d')

        var areaChartData = {
          labels  : ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
          datasets: [
            {
              label               : 'Digital Goods',
              backgroundColor     : 'rgba(60,141,188,0.9)',
              borderColor         : 'rgba(60,141,188,0.8)',
              pointRadius          : false,
              pointColor          : '#3b8bba',
              pointStrokeColor    : 'rgba(60,141,188,1)',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(60,141,188,1)',
              data                : [28, 48, 40, 19, 86, 27, 90]
            },
            {
              label               : 'Electronics',
              backgroundColor     : 'rgba(210, 214, 222, 1)',
              borderColor         : 'rgba(210, 214, 222, 1)',
              pointRadius         : false,
              pointColor          : 'rgba(210, 214, 222, 1)',
              pointStrokeColor    : '#c1c7d1',
              pointHighlightFill  : '#fff',
              pointHighlightStroke: 'rgba(220,220,220,1)',
              data                : [65, 59, 80, 81, 56, 55, 40]
            },
          ]
        }

        var areaChartOptions = {
          maintainAspectRatio : false,
          responsive : true,
          legend: {
            display: false
          },
          scales: {
            xAxes: [{
              gridLines : {
                display : false,
              }
            }],
            yAxes: [{
              gridLines : {
                display : false,
              }
            }]
          }
        }

        // This will get the first returned node in the jQuery collection.
        new Chart(areaChartCanvas, {
          type: 'line',
          data: areaChartData,
          options: areaChartOptions
        })

        //-------------
        //- LINE CHART -
        //--------------
        var lineChartCanvas = $('#lineChart').get(0).getContext('2d')
        var lineChartOptions = $.extend(true, {}, areaChartOptions)
        var lineChartData = $.extend(true, {}, areaChartData)
        lineChartData.datasets[0].fill = false;
        lineChartData.datasets[1].fill = false;
        lineChartOptions.datasetFill = false

        var lineChart = new Chart(lineChartCanvas, {
          type: 'line',
          data: lineChartData,
          options: lineChartOptions
        })

        //-------------
        //- DONUT CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
        var donutData        = {
          labels: [
              'Prioridad Alta',
              'Prioridad Baja',
          ],
          datasets: [
            {
              data: [{{ $prioridad_alta }},{{ $prioridad_baja }}],
              backgroundColor : ['#f56954', '#00c0ef'],
            }
          ]
        }
        var donutOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        var donutData2        = {
          labels: [
              'Municipal',
              'Estatal',
          ],
          datasets: [
            {
              data: [{{ $impacto_municipal }},{{ $impacto_estatal }}],
              backgroundColor : ['#f56954', '#00c0ef'],
            }
          ]
        }
        new Chart(donutChartCanvas, {
          type: 'doughnut',
          data: donutData2,
          options: donutOptions
        })

        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
        var pieData        = donutData;
        var pieOptions     = {
          maintainAspectRatio : false,
          responsive : true,
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        new Chart(pieChartCanvas, {
          type: 'pie',
          data: pieData,
          options: pieOptions
        })

        //-------------
        //- BAR CHART -
        //-------------
        var barChartCanvas = $('#barChart').get(0).getContext('2d')
        var barChartData = $.extend(true, {}, areaChartData)
        var temp0 = areaChartData.datasets[0]
        var temp1 = areaChartData.datasets[1]
        barChartData.datasets[0] = temp1
        barChartData.datasets[1] = temp0

        var barChartOptions = {
          responsive              : true,
          maintainAspectRatio     : false,
          datasetFill             : false
        }

        new Chart(barChartCanvas, {
          type: 'bar',
          data: barChartData,
          options: barChartOptions
        })

        //---------------------
        //- STACKED BAR CHART -
        //---------------------
        var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
        var stackedBarChartData = $.extend(true, {}, barChartData)

        var stackedBarChartOptions = {
          responsive              : true,
          maintainAspectRatio     : false,
          scales: {
            xAxes: [{
              stacked: true,
            }],
            yAxes: [{
              stacked: true
            }]
          }
        }

        new Chart(stackedBarChartCanvas, {
          type: 'bar',
          data: stackedBarChartData,
          options: stackedBarChartOptions
        })
      })
    </script>    
  @endif
@endsection
