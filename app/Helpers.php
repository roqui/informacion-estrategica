<?php

function checkAuth(){
    if (Auth::check()) {
        return true;
    }else{
        return false;
    }
}

function rol($rol){

    if(Auth::user()->rol == $rol){
        return true;
    }

    return false;
}

function getToday(){
    date_default_timezone_set("America/Mexico_City");
    return date('Y-m-d');
}

function getWeek(){
    $ddate = getToday();
    $date = new DateTime($ddate);
    $week = $date->format("W");
    echo "Semana: $week";    
}

function getMunicipios(){

    $municipios = [
         ['Acuitzio','ACUITZIO'],
         ['Aguililla','AGUILILLA'],
         ['Álvaro Obregón','ÁLVARO OBREGÓN'],
         ['Angamacutiro','ANGAMACUTIRO'],
         ['Angangueo','ANGANGUEO'],
         ['Apatzingán','APATZINGÁN'],
         ['Aporo','APORO'],
         ['Aquila','AQUILA'],
         ['Ario','ARIO'],
         ['Arteaga','ARTEAGA'],
         ['Briseñas','BRISEÑAS'],
         ['Buenavista','BUENAVISTA'],
         ['Carácuaro','CARÁCUARO'],
         ['Coahuayana','COAHUAYANA'],
         ['Coalcomán De Vázquez Pallares','COALCOMÁN DE VÁZQUEZ PALLARES'],
         ['Coeneo','COENEO'],
         ['Contepec','CONTEPEC'],
         ['Copándaro','COPÁNDARO'],
         ['Cotija','COTIJA'],
         ['Cuitzeo','CUITZEO'],
         ['Charapan','CHARAPAN'],
         ['Charo','CHARO'],
         ['Chavinda','CHAVINDA'],
         ['Cherán','CHERÁN'],
         ['Chilchota','CHILCHOTA'],
         ['Chinicuila','CHINICUILA'],
         ['Chucándiro','CHUCÁNDIRO'],
         ['Churintzio','CHURINTZIO'],
         ['Churumuco','CHURUMUCO'],
         ['Ecuandureo','ECUANDUREO'],
         ['Epitacio Huerta','EPITACIO HUERTA'],
         ['Erongarícuaro','ERONGARÍCUARO'],
         ['Gabriel Zamora','GABRIEL ZAMORA'],
         ['Hidalgo','HIDALGO'],
         ['La Huacana','LA HUACANA'],
         ['Huandacareo','HUANDACAREO'],
         ['Huaniqueo','HUANIQUEO'],
         ['Huetamo','HUETAMO'],
         ['Huiramba','HUIRAMBA'],
         ['Indaparapeo','INDAPARAPEO'],
         ['Irimbo','IRIMBO'],
         ['Ixtlán','IXTLÁN'],
         ['Jacona','JACONA'],
         ['Jiménez','JIMÉNEZ'],
         ['Jiquilpan','JIQUILPAN'],
         ['Juárez','JUÁREZ'],
         ['Jungapeo','JUNGAPEO'],
         ['Lagunillas','LAGUNILLAS'],
         ['Madero','MADERO'],
         ['Maravatío','MARAVATÍO'],
         ['Marcos Castellanos','MARCOS CASTELLANOS'],
         ['Lázaro Cárdenas','LÁZARO CÁRDENAS'],
         ['Morelia','MORELIA'],
         ['Morelos','MORELOS'],
         ['Múgica','MÚGICA'],
         ['Nahuatzen','NAHUATZEN'],
         ['Nocupétaro','NOCUPÉTARO'],
         ['Nuevo Parangaricutiro','NUEVO PARANGARICUTIRO'],
         ['Nuevo Urecho','NUEVO URECHO'],
         ['Numarán','NUMARÁN'],
         ['Ocampo','OCAMPO'],
         ['Pajacuarán','PAJACUARÁN'],
         ['Panindícuaro','PANINDÍCUARO'],
         ['Parácuaro','PARÁCUARO'],
         ['Paracho','PARACHO'],
         ['Pátzcuaro','PÁTZCUARO'],
         ['Penjamillo','PENJAMILLO'],
         ['Peribán','PERIBÁN'],
         ['La Piedad','LA PIEDAD'],
         ['Purépero','PURÉPERO'],
         ['Puruándiro','PURUÁNDIRO'],
         ['Queréndaro','QUERÉNDARO'],
         ['Quiroga','QUIROGA'],
         ['Cojumatlán De Régules','COJUMATLÁN DE RÉGULES'],
         ['Los Reyes','LOS REYES'],
         ['Sahuayo','SAHUAYO'],
         ['San Lucas','SAN LUCAS'],
         ['Santa Ana Maya','SANTA ANA MAYA'],
         ['Salvador Escalante','SALVADOR ESCALANTE'],
         ['Senguio','SENGUIO'],
         ['Susupuato','SUSUPUATO'],
         ['Tacámbaro','TACÁMBARO'],
         ['Tancítaro','TANCÍTARO'],
         ['Tangamandapio','TANGAMANDAPIO'],
         ['Tangancícuaro','TANGANCÍCUARO'],
         ['Tanhuato','TANHUATO'],
         ['Taretan','TARETAN'],
         ['Tarímbaro','TARÍMBARO'],
         ['Tepalcatepec','TEPALCATEPEC'],
         ['Tingambato','TINGAMBATO'],
         ['Tinguindín','TINGUINDÍN'],
         ['Tiquicheo De Nicolás Romero','TIQUICHEO DE NICOLÁS ROMERO'],
         ['Tlalpujahua','TLALPUJAHUA'],
         ['Tlazazalca','TLAZAZALCA'],
         ['Tocumbo','TOCUMBO'],
         ['Tumbiscatío','TUMBISCATÍO'],
         ['Turicato','TURICATO'],
         ['Tuxpan','TUXPAN'],
         ['Tuzantla','TUZANTLA'],
         ['Tzintzuntzan','TZINTZUNTZAN'],
         ['Tzitzio','TZITZIO'],
         ['Uruapan','URUAPAN'],
         ['Venustiano Carranza','VENUSTIANO CARRANZA'],
         ['Villamar','VILLAMAR'],
         ['Vista Hermosa','VISTA HERMOSA'],
         ['Yurécuaro','YURÉCUARO'],
         ['Zacapu','ZACAPU'],
         ['Zamora','ZAMORA'],
         ['Zináparo','ZINÁPARO'],
         ['Zinapécuaro','ZINAPÉCUARO'],
         ['Ziracuaretiro','ZIRACUARETIRO'],
         ['Zitácuaro','ZITÁCUARO'],
         ['José Sixto Verduzco','JOSÉ SIXTO VERDUZCO']
    ];

    return $municipios;
}


function getDependencias(){

    $dependencias = [
        'Secretaría de Gobierno',
        'Secretaría de Finanzas y Administración',
        'Secretaría de Contraloría',
        'Secretaría de Seguridad Pública',
        'Secretaría de Desarrollo Económico',
        'Secretaría de Turismo',
        'Secretaría de Desarrollo Rural y Agroalimentario',
        'Secretaría de Comunicaciones y Obras Públicas',
        'Secretaría de Medio Ambiente',
        'Secretaria de Desarrollo Urbano y Movilidad',
        'Secretaría de Educación',
        'Secretaría de Cultura',
        'Secretaría de Salud',
        'Secretaría del Bienestar',
        'Secretaria del Migrante',
        'Secretaría de Igualdad Sustantiva y Desarrollo de las Mujeres Michoacanas'
    ];


    // $dependencias = [
    //     'SECRETARÍA DE ADMINISTRACIÓN Y FINANZAS DEL CONGRESO DEL ESTADO DE MICHOACÁN',
    //     'SECRETARÍA ADMINISTRATIVA DEL SUPREMO TRIBUNAL DE JUSTICIA',
    //     'AUDITORÍA SUPERIOR DE MICHOACÁN',
    //     'GOBERNATURA DEL ESTADO DE MICHOACÁN',
    //     'COORDINACÍON GENERAL DE COMUNICACIÓN SOCIAL DEL ESTADO DE MICHOACÁN',
    //     'Secretaría de Gobierno',
    //     'Secretaría de Finanzas y Administración',
    //     'Secretaría de Comunicaciones y Obras Públicas',
    //     'SECRETARÍA DE DESARROLLO RURAL Y AGROALIMENTARIO DEL ESTADO DE MICHOACÁN',
    //     'Secretaría de Desarrollo Económico',
    //     'Secretaría de Turismo',
    //     'Secretaría de Educación',
    //     'Secretaría del Bienestar',
    //     'Secretaria del Migrante',
    //     'Secretaría de Agricultura y Desarrollo Rural',
    //     'Secretaría de Seguridad Pública',
    //     'Secretaría de Salud',
    //     'Secretaría de Medio Ambiente, Cambio Climático y Desarrollo Territorial',
    //     'Secretaría del Medio Ambiente',
    //     'Secretaría de Contraloría',
    //     'Secretaría de Desarrollo Urbano y Movilidad',
    //     'SECRETARÍA DE DESARROLLO SOCIAL Y HUMANO DEL ESTADO DE MICHOACÁN',
    //     'Secretaría de Cultura',
    //     'SECRETARIADO EJECUTIVO DEL SISTEMA ESTATAL DE SEGURIDAD PÚBLICA  DEL ESTADO DE MICHOACÁN',
    //     'Secretaría de Igualdad Sustantiva y Desarrollo de las Mujeres Michoacanas',
    //     'CONSEJO JURÍDICO DEL EJECUTIVO DEL ESTADO DE MICHOACÁN DE OCAMPO',
    //     'TRIBUNAL DE CONCILIACIÓN Y ARBITRAJE DEL ESTADO DE MICHOACÁN',
    //     'JUNTA LOCAL DE CONCILIACIÓN Y ARBITRAJE DEL ESTADO DE MICHOACÁN',
    //     'SECRETARÍA EJECUTIVA DEL SISTEMA ESTATAL DE PROTECCIÓN INTEGRAL DE NIÑAS, NIÑOS Y ADOLESCENTES DEL ESTADO DE MICHOACÁN',
    //     'COORDINACIÓN DEL SISTEMA PENITENCIARIO DEL ESTADO DE MICHOACÁN',
    //     'INSTITUTO REGISTRAL Y CATASTRAL DEL ESTADO DE MICHOACÁN DE OCAMPO',
    //     'PROCURADURÍA DE PROTECCIÓN AL AMBIENTE DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN COORDINADORA DEL TRANSPORTE PÚBLICO DE MICHOACÁN',
    //     'INSTITUTO MICHOACANO DE CIENCIAS DE LA EDUCACIÓN JOSÉ MA. MORELOS',
    //     'ORQUESTA SINFÓNICA DE MICHOACÁN',
    //     'COMISIÓN ESTATAL PARA LA PROTECCIÓN CONTRA RIESGOS SANITARIOS',
    //     'INSTITUTO DEL ARTESANO MICHOACANO',
    //     'COMISIÓN ESTATAL DE CULTURA FÍSICA Y DEPORTE',
    //     'SISTEMA MICHOACANO DE RADIO Y TELEVISIÓN',
    //     'CENTRO DE CONVENCIONES DE MORELIA',
    //     'PARQUE ZOOLÓGICO BENITO JUÁREZ DELEGADO ADMINISTRATIVO',
    //     'SISTEMA PARA EL DESARROLLO INTEGRAL DE LA FAMILIA',
    //     'UNIVERSIDAD VIRTUAL DEL ESTADO DE MICHOACÁN',
    //     'TELEBACHILLERATO MICHOACÁN',
    //     'INSTITUTO DE VIVIENDA DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN FORESTAL DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN DE PESCA DEL ESTADO DE MICHOACÁN',
    //     'COLEGIO DE BACHILLERES DEL ESTADO DE MICHOACÁN',
    //     'COLEGIO DE EDUCACIÓN PROFESIONAL TÉCNICAL DEL ESTADO DE MICHOACÁN',
    //     'UNIVERSIDAD TECNOLÓGICA DE MORELIA',
    //     'COLEGIO DE ESTUDIOS CIENTÍFICOS Y TECNOLÓGICOS DEL ESTADO DE MICHOACÁN',
    //     'INSTITUTO DE CAPACITACIÓN PARA EL TRABAJO DEL ESTADO DE MICHOACÁN',
    //     'INSTITUTO DE LA INFRAESTRUCTURA FÍSICA EDUCATIVA DEL ESTADO DE MICHOACÁN',
    //     'UNIVERSIDAD DE LA CIÉNEGA DEL ESTADO DE MICHOACÁN',
    //     'CENTRO ESTATAL DE CERTIFICACIÓN, ACREDITACIÓN Y CONTROL DE CONFIANZA',
    //     'UNIVERSIDAD INTERCULTURAL INDÍGENA DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN ESTATAL DE ARBITRAJE MÉDICO DEL ESTADO DE MICHOACÁN',
    //     'JUNTA DE ASISTENCIA PRIVADA DEL ESTADO DE MICHOACAN',
    //     'COMISIÓN ESTATAL PARA EL DESARROLLO DE LOS PUEBLOS INDÍGENAS',
    //     'INSTITUTO DE PLANEACIÓN DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN ESTATAL DEL AGUA Y GESTIÓN DE CUENCAS',
    //     'COMITÉ DE ADQUISICIONES DEL PODER EJECUTIVO',
    //     'UNIVERSIDAD POLITÉCNICA DE LÁZARO CÁRDENAS',
    //     'UNIVERSIDAD POLITÉCNICA DE URUAPAN',
    //     'INSTITUTO DE DEFENSORÍA PÚBLICA DEL ESTADO DE MICHOACÁN',
    //     'INSTITUTO ESTATAL DE ESTUDIOS SUPERIORES EN SEGURIDAD Y PROFESIONALIZACIÓN POLICIAL DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN EJECUTIVA DE ATENCIÓN A VÍCTIMAS DEL ESTADO DE MICHOACÁN',
    //     'CENTRO ESTATAL DE FOMENTO GANADERO',
    //     'GENERAL DE SISTEMA INTEGRAL DE FINANCIAMIENTO PARA EL DESARROLLO DE MICHOACÁN',
    //     'GENERAL DEL INSTITUTO DE LA JUVENTUD MICHOACANA',
    //     'INSTITUTO DE CIENCIA, TECNOLOGÍA E INNOVACIÓN DEL ESTADO DE MICHOACÁN',
    //     'CONSEJO ESTATAL PARA PREVENIR Y ELIMINAR LA DISCRIMINACIÓN Y LA VIOLENCIA',
    //     'UNIVERSIDAD TECNOLÓGICA DEL ORIENTE',
    //     'SECRETARÍA EJECUTIVA DEL SISTEMA ESTATAL ANTICORRUPCIÓN',
    //     'UNIVERSIDAD MICHOACANA DE SAN NICOLÁS DE HIDALGO',
    //     'INSTITUTO ELECTORAL DE MICHOACÁN',
    //     'TRIBUNAL ELECTORAL DEL ESTADO DE MICHOACÁN',
    //     'TRIBUNAL DE JUSTICIA ADMINISTRATIVA DEL ESTADO DE MICHOACÁN',
    //     'COMISIÓN ESTATAL DE DERECHOS HUMANOS',
    //     'INSTITUTO MICHOACANO DE TRANSPARENCIA, ACCESO A LA INFORMACIÓN PÚBLICA Y PROTECCIÓN DE DATOS PERSONALES',
    //     'FISCALÍA GENERAL DEL ESTADO DE MICHOACÁN',
    //     'JUNTA DE CAMINOS DEL ESTADO DE MICHOACÁN',
    //     'RÉGIMEN ESTATAL DE PROTECCIÓN SOCIAL EN SALUD DE MICHOACÁN DE OCAMPO',
    // ];

    return $dependencias;

}