<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InformacionEstrategica;

class PlantillaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function plantilla(){

        // return InformacionEstrategica::all();

        if(rol('administrador')){
            $registros = InformacionEstrategica::select('*')->get();
            $cuenta_registros   =   $registros->count();
            $prioridad_alta     =   $registros->where('prioridad','alta')->count();
            $prioridad_baja     =   $registros->where('prioridad','baja')->count();
    
            $impacto_estatal       =   $registros->where('impacto_probable','Estatal')->count();
            $impacto_municipal     =   $registros->where('impacto_probable','Municipal')->count();
            $registros = InformacionEstrategica::select('*')->orderByDesc('id')->get();
    
            return view('plantilla/dashboard',[
                'registros'         =>  $registros, 
                'cuenta_registros'  =>  $cuenta_registros,
                'prioridad_baja'    =>  $prioridad_baja,
                'prioridad_alta'    =>  $prioridad_alta,
                'impacto_estatal'   =>  $impacto_estatal,
                'impacto_municipal' =>  $impacto_municipal
            ]);
        }else{
            return view('plantilla/dashboard');
        }
    }
}
