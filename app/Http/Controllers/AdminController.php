<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InformacionEstrategica;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function show(Request $request){
       

        if ($request->dependencia != null){

            $dep = $request->dependencia;
               if(rol('administrador')){
               $registros = InformacionEstrategica::all()->where('dependencia',$request->dependencia);
           }else{
               $registros = InformacionEstrategica::all()->where('id_usuario',Auth::user()->id);
           }
       
           return view('admin/registros',['registros' => $registros, 'dep' => $dep]); 
       
        }else{
       if(rol('administrador')){
           $dep = "-- Seleccione una dependencia --";
                   $registros = InformacionEstrategica::all();
                   return view('admin/registros',['registros' => $registros, 'dep' => $dep]); 
               }else{
                   $registros = InformacionEstrategica::all()->where('id_usuario',Auth::user()->id);
                   return view('admin/registros',['registros' => $registros]); 
               }
       
       
        }
    }
        
    public function create(Request $request){
        // return $request; 
        $request->validate([
            'dependencia'               =>  'required',
            'fecha'                     =>  'required',
            'periodo'                   =>  'required',
            'responsable_validacion'    =>  'required',
            'prioridad'                 =>  'required',
            'tema'                      =>  'required',
            'responsable_atencion'      =>  'required',
            'problematica'              =>  'required',
            'areas'                     =>  'required',
            'organizaciones'            =>  'required',
            'seguimiento'               =>  'required',
            'propuestas'                =>  'required',
            'recursos'                  =>  'required',
            'impacto'                   =>  'required',
            'impacto_dependencia'       =>  'exclude_if:impacto,Estatal|required',
            'riesgos'                   =>  'required',
        ]);

        try{
            
            $request->impacto == "Estatal" ? $impacto_depedencia = null : $impacto_depedencia = $request->impacto_dependencia;

            $originalname_problematica   =   null;
            $path_problematica           =   null;
            $url_problematica            =   null;

            $originalname_propuestas   =   null;
            $path_propuestas           =   null;
            $url_propuestas            =   null;

            
            if($request->hasFile('archivo_problematica')){
                $originalname_problematica = $request->file('archivo_problematica')->getClientOriginalName();
                $path_problematica = $request->file('archivo_problematica')->storeAs('public/documentos/'.Auth::user()->name, $originalname_problematica);
                $url_problematica = Storage::url($path_problematica);
            }

            if($request->hasFile('archivo_propuestas')){
                $originalname_propuestas = $request->file('archivo_propuestas')->getClientOriginalName();
                $path_propuestas = $request->file('archivo_propuestas')->storeAs('public/documentos/'.Auth::user()->name, $originalname_propuestas);
                $url_propuestas = Storage::url($path_propuestas);
            }

            InformacionEstrategica::create([
                'dependencia'               =>  $request->dependencia,
                'fecha'                     =>  $request->fecha,
                'periodo_reportado'         =>  $request->periodo,
                'responsable_validacion'    =>  $request->responsable_validacion,
                'prioridad'                 =>  $request->prioridad,
                'tema'                      =>  $request->tema,
                'responsable_atencion'      =>  $request->responsable_atencion,
                'problematica'              =>  $request->problematica,
                // PROBLEMATICA
                'ruta_problematica'         =>  $path_problematica,
                'nombre_problematica'       =>  $originalname_problematica,
                'url_problematica'          =>  $url_problematica,
                // FIN PROBLEMATICA
                'areas'                     =>  $request->areas,
                'organizaciones'            =>  $request->organizaciones,
                'seguimiento'               =>  $request->seguimiento,
                'propuestas'                =>  $request->propuestas,
                // PROPUESTAS DE ACCION
                'ruta_propuestas'           =>  $path_propuestas,
                'nombre_propuestas'         =>  $originalname_propuestas,
                'url_propuestas'            =>  $url_problematica,
                // FIN PROPUESTAS DE ACCION
                'recursos_financieros'      =>  $request->recursos,
                'impacto_probable'          =>  $request->impacto,
                'impacto_dependencia'       =>  $impacto_depedencia,
                'id_usuario'                =>  Auth::user()->id,
                'riesgos'                   =>  $request->riesgos,
            ]);

        }catch(Exception $ex){
            return redirect()->back()->with('error','Algo ha salido mal. Intenta más tarde.');

        }

        return redirect()->back()->with('success','Formulario registrado correctamente');
    }

    public function downloadFile(Request $request){

        return response()->download(storage_path("app/{$request->path}"));
    }
}