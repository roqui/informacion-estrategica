<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class InvestigacionController extends Controller
{
    public function formularioTJAM(){

        return view('Investigacion/expedientesTJAM');
    }

    public function formularioExpedientes(){

        return view('Investigacion/expedientes');
    }
}
