<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\InformacionEstrategica;
use App\Models\User;


class InformacionEstrategicaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    
    public function formulario(Request $request){

        $dependencias = User::select('id','name')->where('tipo','dependencia');

        return view('formulario_informacion/informacion_estrategica');
    }


    
}
