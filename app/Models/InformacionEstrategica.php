<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InformacionEstrategica extends Model
{
    protected $fillable = [
        'dependencia',
        'fecha',
        'periodo_reportado',
        'responsable_validacion',
        'prioridad',
        'tema',
        'responsable_atencion',
        'problematica',
        // PROBLEMATICA
        'ruta_problematica',
        'nombre_problematica',
        'url_problematica',
        // FIN PROBLEMATICA
        'areas',
        'organizaciones',
        'seguimiento',
        'propuestas',
        // PROPUESTAS DE ACCION
        'ruta_propuestas',
        'nombre_propuestas',
        'url_propuestas',
        // FIN PROPUESTAS DE ACCION
        'recursos_financieros',
        'impacto_probable',
        'impacto_dependencia',
        'id_usuario',
        'riesgos'

    ];

    protected $table = 'informacion_estrategica';
}
