<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      =>  'admin',
            'username'  =>  'admin_2021',
            'rol'       =>  'administrador',
            // 'password'  =>  Hash::make('holamundo')
            'password'  =>  Hash::make('Admin1232021')
        ]);

        User::create([
            'name'      =>  'Administrador Michoacán',
            'username'  =>  'admin_Mich',
            'rol'       =>  'administrador',
            'password'  =>  Hash::make('Admin_Mich2021')
        ]);

        User::create([
            'name'      =>  'Coordinador de Estrategia',
            'username'  =>  'Coordin_Estrat',
            'rol'       =>  'administrador',
            'password'  =>  Hash::make('Coordin_Estrat2021')
        ]);

        User::create([
            'name'      =>  'Secretaría de Gobierno',
            'username'  =>  'sec_gobierno',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecGob2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Finanzas y Administración',
            'username'  =>  'sec_finanzas_admin',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecFin2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Contraloría',
            'username'  =>  'sec_contraloria',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecCont2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Seguridad Pública',
            'username'  =>  'sec_seguridad_publica',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecSegPublica2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Desarrollo Económico',
            'username'  =>  'sec_des_economico',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecDesEconomico2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Turismo',
            'username'  =>  'sec_turismo',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecTur2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Agricultura y Desarrollo Rural',
            'username'  =>  'sec_agricultura',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecAgr2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Comunicaciones y Obras Públicas',
            'username'  =>  'sec_obras',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecComOb2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Medio Ambiente, Cambio Climático y Desarrollo Territorial',
            'username'  =>  'sec_medio_ambiente',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecMACCDT2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Educación',
            'username'  =>  'sec_educacion',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecEdu2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Cultura',
            'username'  =>  'sec_cultura',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecCul2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Salud',
            'username'  =>  'sec_salud',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecSalud2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría del Bienestar',
            'username'  =>  'sec_bienestar',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecBen2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaria del Migrante',
            'username'  =>  'sec_migrante',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecMig2021Mich')
        ]);
        User::create([
            'name'      =>  'Secretaría de Igualdad Sustantiva y Desarrollo de las Mujeres Michoacanas',
            'username'  =>  'sec_igualdad',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecISDMM2021Mich')
        ]);
        
        
    }
}
