<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class UsersTableSeederV2 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'      =>  'Secretaría del Medio Ambiente',
            'username'  =>  'sec_medio_amb2',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecMA2021')
        ]);

        User::create([
            'name'      =>  'Secretaría de Desarrollo Urbano y Movilidad',
            'username'  =>  'sec_desarrollo_urbano',
            'rol'       =>  'general',
            'password'  =>  Hash::make('SecUrb2021')
        ]);

        
    }
}
