<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformacionEstrategicaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informacion_estrategica', function (Blueprint $table) {
            $table->id();
            $table->string('dependencia');
            $table->string('fecha');
            $table->string('periodo_reportado');
            $table->string('responsable_validacion');
            $table->string('prioridad');
            $table->string('tema');
            $table->string('responsable_atencion');
            $table->longText('problematica');

            // PROBLEMATICA
            $table->string('ruta_problematica')->nullable()->comment('Ruta del archivo de problemática');
            $table->string('nombre_problematica')->nullable()->comment('Nombre del archivo de problemática');
            $table->string('url_problematica')->nullable()->comment('Url del archivo de problemática');
            // FIN PROBLEMATICA

            $table->longText('areas');
            $table->longText('organizaciones');
            $table->longText('seguimiento');
            $table->longText('propuestas');

            // PROPUESTAS DE ACCION
            $table->string('ruta_propuestas')->nullable()->comment('Ruta del archivo de propuestas de acción');
            $table->string('nombre_propuestas')->nullable()->comment('Nombre del archivo de propuestas de acción');
            $table->string('url_propuestas')->nullable()->comment('Url del archivo de propuestas de acción');
            // FIN PROPUESTAS DE ACCION

            $table->longText('recursos_financieros');
            $table->string('impacto_probable');
            $table->string('impacto_dependencia')->nullable();
            $table->unsignedBigInteger('id_usuario');
            
            $table->timestamps();

            $table->foreign('id_usuario')->references('id')->on('users');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informacion_estrategica');
    }
}
