<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlantillaController;
use App\Http\Controllers\InvestigacionController;
use App\Http\Controllers\SustanciacionController;
use App\Http\Controllers\InformacionEstrategicaController;
use App\Http\Controllers\AdminController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PlantillaController::class, 'plantilla'])->name('dashboard');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// Vistas y métodos públicos
Route::get('/formulario/IE', [InformacionEstrategicaController::class, 'formulario'])->name('formulario_ie');

// Vistas y métodos privados
Route::post('/informacion_estrategica/captura', [AdminController::class, 'create'])->name('formulario_ie_captura');
Route::get('/informacion_estrategica/registros/{dependencia?}', [AdminController::class, 'show'])->name('registros');
Route::post("/informacion_estrategica/registros/descargar_archivo", [AdminController::class, 'downloadFile'] )->name('download_file');




